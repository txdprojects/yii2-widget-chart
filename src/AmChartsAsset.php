<?php

namespace txd\widgets\chart;

use yii\web\AssetBundle;

class AmChartsAsset extends AssetBundle
{
	/**
	 * @inheritdoc
	 */
	public $js = [
		'js/core.js',
		'js/charts.js',
		'js/maps.js',
		'js/geodata/worldLow.js',
	];

	/**
	 * @inheritdoc
	 */
	public $depends = [
		'yii\web\JqueryAsset',
	];

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->sourcePath = __DIR__ . '/assets/amcharts';
	}
}
